const Categories = require("../models/categories.model.js");

// Create and Save a new categories
exports.create = (req, res) => {
	if (!req.body) {
		res.status(400).send({
			message: "Content can not be empty!",
		});
	}

	// Create a categories
	const categories = new Categories({
		nama_kategori: req.body.nama_kategori,
	});

	// Save Categories in the database
	Categories.create(categories, (err, data) => {
		if (err)
			res.status(500).send({
				message:
					err.message ||
					"Some error occurred while creating the Categories.",
			});
		else res.send(data);
	});
};

// Retrieve all Categories from the database.
exports.findAll = (req, res) => {
	Categories.getAll((err, data) => {
		if (err)
			res.status(500).send({
				message:
					err.message ||
					"Some error occurred while retrieving categories.",
			});
		else res.send(data);
	});
};

// Find a single categories with a categoriesId
exports.findOne = (req, res) => {
	Categories.findById(req.params.categoriesId, (err, data) => {
		if (err) {
			if (err.kind === "not_found") {
				res.status(404).send({
					message: `Not found Product with id_kategori ${req.params.categoriesId}.`,
				});
			} else {
				res.status(500).send({
					message:
						"Error retrieving Categories with id_kategori " +
						req.params.categoriesId,
				});
			}
		} else res.send(data);
	});
};

// Update a Categories identified by the categoriesId in the request
exports.update = (req, res) => {
	if (!req.body) {
		res.status(400).send({
			message: "Content can not be empty!",
		});
	}

	Categories.updateById(
		req.params.categoriesId,
		new Categories(req.body),
		(err, data) => {
			if (err) {
				if (err.kind === "not_found") {
					res.status(404).send({
						message: `Not found categories with id ${req.params.categoriesId}.`,
					});
				} else {
					res.status(500).send({
						message:
							"Error updating categories with id " +
							req.params.categoriesId,
					});
				}
			} else res.send(data);
		}
	);
};

// Delete a Categories with the specified categoriId in the request
exports.delete = (req, res) => {
	Categories.remove(req.params.categoriesId, (err, data) => {
		if (err) {
			if (err.kind === "not_found") {
				res.status(404).send({
					message: `Not found product with id_kategori ${req.params.categoriesId}.`,
				});
			} else {
				res.status(500).send({
					message:
						"Could not delete product with id_kategori " +
						req.params.categoriesId,
				});
			}
		} else res.send({ message: `Categories was deleted successfully!` });
	});
};
