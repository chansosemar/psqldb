const bcrypt = require("bcrypt");
const jwt = require("jsonwebtoken");
require("dotenv").config();

const Helper = {
	hashPassword(password) {
		return bcrypt.hashSync(password, bcrypt.genSaltSync(8));
	},

	comparePassword(hashPassword, password) {
		return bcrypt.compareSync(password, hashPassword);
	},

	generateToken(id) {
		const token = jwt.sign(
			{
				userId: id,
			},
			process.env.SECRET,
			{ expiresIn: "7d" }
		);
		return token;
	},

	verifyToken(request, response, next) {
		let token = request.headers.authorization;

		if (!token) {
			return response.status(400).send({ message: "Unauthorized" });
		}

		jwt.verify(
			token.replace(/^Bearer\s+/, ""),
			process.env.SECRET,
			(error, decoded) => {
				if (error) {
					return response.status(401).send({
						message: "Unauthorized!",
					});
				}
				request.userId = decoded.id;
				next();
			}
		);
	},
};

module.exports = {
	Helper,
};
