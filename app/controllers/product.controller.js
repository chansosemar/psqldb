const Product = require("../models/product.model.js");

// Create and Save a new product
exports.create = (req, res) => {
	if (!req.body) {
		res.status(400).send({
			message: "Content can not be empty!",
		});
	}

	// Create a product
	const product = new Product({
		id_kategori: req.body.id_kategori,
		nama_produk: req.body.nama_produk,
		kode_produk: req.body.kode_produk,
		foto_produk: req.body.foto_produk,
	});

	// Save Product in the database
	Product.create(product, (err, data) => {
		if (err)
			res.status(500).send({
				message:
					err.message ||
					"Some error occurred while creating the Product.",
			});
		else res.send(data);
	});
};

// Retrieve all Product from the database.
exports.findAll = (req, res) => {
	Product.getAll((err, data) => {
		if (err)
			res.status(500).send({
				message:
					err.message ||
					"Some error occurred while retrieving product.",
			});
		else res.send(data);
	});
};

// Find a single Product with a productId
exports.findOne = (req, res) => {
	Product.findById(req.params.productId, (err, data) => {
		if (err) {
			if (err.kind === "not_found") {
				res.status(404).send({
					message: `Not found Product with id_produk ${req.params.productId}.`,
				});
			} else {
				res.status(500).send({
					message:
						"Error retrieving Product with id_produk " +
						req.params.productId,
				});
			}
		} else res.send(data);
	});
};

// Update a Product identified by the productId in the request
exports.update = (req, res) => {
	if (!req.body) {
		res.status(400).send({
			message: "Content can not be empty!",
		});
	}

	Product.updateById(
		req.params.productId,
		new Product(req.body),
		(err, data) => {
			if (err) {
				if (err.kind === "not_found") {
					res.status(404).send({
						message: `Not found Product with id ${req.params.productId}.`,
					});
				} else {
					res.status(500).send({
						message:
							"Error updating product with id " +
							req.params.productId,
					});
				}
			} else res.send(data);
		}
	);
};

// Delete a product with the specified productId in the request
exports.delete = (req, res) => {
	Product.remove(req.params.productId, (err, data) => {
		if (err) {
			if (err.kind === "not_found") {
				res.status(404).send({
					message: `Not found product with id_produk ${req.params.productId}.`,
				});
			} else {
				res.status(500).send({
					message:
						"Could not delete product with id_produk " +
						req.params.productId,
				});
			}
		} else res.send({ message: `Product was deleted successfully!` });
	});
};
