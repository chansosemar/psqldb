const Stock = require("../models/stock.model.js");

// Create and Save a new stock
exports.create = (req, res) => {
	if (!req.body) {
		res.status(400).send({
			message: "Content can not be empty!",
		});
	}

	// Create a stock
	const stock = new Stock({
		id_produk: req.body.id_produk,
		jumlah_barang: req.body.jumlah_barang,
	});

	// Save stock in the database
	Stock.create(stock, (err, data) => {
		if (err)
			res.status(500).send({
				message:
					err.message ||
					"Some error occurred while creating the Stock.",
			});
		else res.send(data);
	});
};

// Retrieve all stock from the database.
exports.findAll = (req, res) => {
	Stock.getAll((err, data) => {
		if (err)
			res.status(500).send({
				message:
					err.message ||
					"Some error occurred while retrieving stock.",
			});
		else res.send(data);
	});
};

// Find a single stock with a stockId
exports.findOne = (req, res) => {
	Stock.findById(req.params.stockId, (err, data) => {
		if (err) {
			if (err.kind === "not_found") {
				res.status(404).send({
					message: `Not found Stock with id_stok ${req.params.stockId}.`,
				});
			} else {
				res.status(500).send({
					message:
						"Error retrieving Stock with id_stok " +
						req.params.stockId,
				});
			}
		} else res.send(data);
	});
};

// Update a stock identified by the stockId in the request
exports.update = (req, res) => {
	if (!req.body) {
		res.status(400).send({
			message: "Content can not be empty!",
		});
	}

	Stock.updateById(
		req.params.stockId,
		new Stock(req.body),
		(err, data) => {
			if (err) {
				if (err.kind === "not_found") {
					res.status(404).send({
						message: `Not found Stock with id ${req.params.stockId}.`,
					});
				} else {
					res.status(500).send({
						message:
							"Error updating stock with id " +
							req.params.stockId,
					});
				}
			} else res.send(data);
		}
	);
};

// Delete a stock with the specified stockId in the request
exports.delete = (req, res) => {
	Stock.remove(req.params.stockId, (err, data) => {
		if (err) {
			if (err.kind === "not_found") {
				res.status(404).send({
					message: `Not found stock with id_stok ${req.params.stockId}.`,
				});
			} else {
				res.status(500).send({
					message:
						"Could not delete stock with id_stok " +
						req.params.stockId,
				});
			}
		} else res.send({ message: `Stock was deleted successfully!` });
	});
};
