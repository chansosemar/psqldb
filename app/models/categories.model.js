const { pool } = require("./db.js");

const getCategories = (request, response) => {
	pool.query(
		"SELECT * FROM tbl_kategori ORDER BY id_kategori ASC",
		(error, results) => {
			if (error) {
				throw error;
			}
			response.status(200).json(results.rows);
		}
	);
};

const getCategorieById = (request, response) => {
	const id = parseInt(request.params.categoriesId);

	pool.query(
		"SELECT * FROM tbl_kategori LEFT JOIN tbl_produk ON id_kategori = kategori_id WHERE id_kategori = $1",
		[id],
		(error, results) => {
			if (error) {
				throw error;
			}
			response.status(200).json(results.rows);
		}
	);
};

const createCategorie = (request, response) => {
	const {nama_kategori } = request.body;

	pool.query(
		`INSERT INTO tbl_kategori (nama_kategori ) VALUES ($1)`,
		[nama_kategori],
		(error, results) => {
			if (error) {
				throw error;
			}

			response.status(201).send(`Categorie added successfully`);
		}
	);
};

const updateCategorie = (request, response) => {
	const id = parseInt(request.params.categoriesId);
	const { nama_kategori } = request.body;

	pool.query(
		"UPDATE tbl_kategori SET nama_kategori = $1 WHERE id_kategori = $2",
		[nama_kategori, id],
		(error, results) => {
			if (error) {
				throw error;
			}
			response.status(200).send(`Categorie modified with ID: ${id}`);
		}
	);
};

const deleteCategorie = (request, response) => {
	const id = parseInt(request.params.categoriesId);

	pool.query(
		"DELETE FROM tbl_kategori WHERE id_kategori = $1",
		[id],
		(error, results) => {
			if (error) {
				throw error;
			}
			response.status(200).send(`Categorie deleted successfully`);
		}
	);
};

module.exports = {
	getCategories,
	getCategorieById,
	createCategorie,
	updateCategorie,
	deleteCategorie,
};
