const { pool } = require("./db.js");

const getProducts = (request, response) => {
	pool.query(
		"SELECT * FROM tbl_produk LEFT JOIN tbl_stok ON id_produk = produk_id LEFT JOIN tbl_kategori ON kategori_id = id_kategori ORDER BY id_produk ASC",
		(error, results) => {
			if (error) {
				throw error;
			}
			response.status(200).json(results.rows);
			response.status(200).send(results.rows);
		}
	);
};

const getProductById = (request, response) => {
	const id = parseInt(request.params.productId);

	pool.query(
		"SELECT * FROM tbl_produk LEFT JOIN tbl_stok ON id_produk = produk_id WHERE id_produk = $1",
		[id],
		(error, results) => {
			if (error) {
				throw error;
			}
			response.status(200).json(results.rows);
		}
	);
};

const createProduct = (request, response) => {
	const { kategori_id, nama_produk, kode_produk, foto_produk } = request.body;

	pool.query(
		`INSERT INTO tbl_produk (kategori_id, nama_produk, kode_produk, foto_produk ) VALUES ($1, $2, $3, $4) RETURNING *`,
		[kategori_id, nama_produk, kode_produk, foto_produk],
		(error, results) => {
			if (error) {
				throw error;
			}
			response.status(201).json(results.rows);
			const id = results.rows[0].id_produk;
			const jml = 0;
			const text = `INSERT INTO tbl_stok (produk_id, jumlah_barang ) VALUES ($1, $2)`;
			pool.query(text, [id, jml], (error, results) => {
				if (error) {
					console.log(error);
				}
				console.log("BERHASIL");
			});
		}
	);
};

const updateProduct = (request, response) => {
	const id = parseInt(request.params.productId);
	const { kategori_id, nama_produk, kode_produk, foto_produk } = request.body;

	pool.query(
		"UPDATE tbl_produk SET kategori_id = $1, nama_produk = $2, kode_produk = $3, foto_produk = $4 WHERE id_produk = $5",
		[kategori_id, nama_produk, kode_produk, foto_produk, id],
		(error, results) => {
			if (error) {
				throw error;
			}
			response.status(200).send(`product modified with ID: ${id}`);
		}
	);
};

const deleteProduct = (request, response) => {
	const id = parseInt(request.params.productId);

	pool.query(
		"DELETE FROM tbl_produk WHERE id_produk = $1",
		[id],
		(error, results) => {
			if (error) {
				throw error;
			}
			response.status(200).send(`product deleted successfully`);
		}
	);
};

module.exports = {
	getProducts,
	getProductById,
	createProduct,
	updateProduct,
	deleteProduct,
};
