const { pool } = require("./db.js");

const getStocks = (request, response) => {
	pool.query(
		"SELECT * FROM tbl_stok ORDER BY id_stok ASC",
		(error, results) => {
			if (error) {
				throw error;
			}
			response.status(200).json(results.rows);
		}
	);
};

const getStockById = (request, response) => {
	const id = parseInt(request.params.stockId);

	pool.query(
		"SELECT * FROM tbl_stok WHERE id_stok = $1",
		[id],
		(error, results) => {
			if (error) {
				throw error;
			}
			response.status(200).json(results.rows);
		}
	);
};

const createStock = (request, response) => {
	const { id_produk, jumlah_barang } = request.body;

	pool.query(
		`INSERT INTO tbl_stok (id_produk, jumlah_barang ) VALUES ($1, $2)`,
		[id_produk, jumlah_barang],
		(error, results) => {
			if (error) {
				throw error;
			}

			response.status(201).send(`Stock added successfully`);
		}
	);
};

const updateStock = (request, response) => {
	const id = parseInt(request.params.stockId);
	const { id_produk, jumlah_barang } = request.body;

	pool.query(
		"UPDATE tbl_stok SET id_produk = $1, jumlah_barang = $2 WHERE id_stok = $3",
		[id_produk, jumlah_barang, id],
		(error, results) => {
			if (error) {
				throw error;
			}
			response.status(200).send(`Stock modified with ID: ${id}`);
		}
	);
};

const deleteStock = (request, response) => {
	const id = parseInt(request.params.stockId);

	pool.query(
		"DELETE FROM tbl_stok WHERE id_stok = $1",
		[id],
		(error, results) => {
			if (error) {
				throw error;
			}
			response.status(200).send(`Stock deleted successfully`);
		}
	);
};

module.exports = {
	getStocks,
	getStockById,
	createStock,
	updateStock,
	deleteStock,
};
