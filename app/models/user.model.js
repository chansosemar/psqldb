const { pool } = require("./db.js");
const { Helper } = require("../controllers/helper.js");

const getUsers = (request, response) => {
  pool.query(
    "SELECT * FROM tbl_user ORDER BY id_user ASC",
    (error, results) => {
      if (error) {
        throw error;
      }
      response.status(200).json(results.rows);
    }
  );
};

const getUserById = (request, response) => {
  const id = parseInt(request.params.userId);

  pool.query(
    "SELECT * FROM tbl_user WHERE id_user = $1",
    [id],
    (error, results) => {
      if (error) {
        console.log(error);
      }
      response.status(200).json(results.rows);
    }
  );
};

const createUser = (request, response) => {
  const hashPassword = Helper.hashPassword(request.body.password);
  const { nama_user, password } = request.body;

  pool.query(
    `INSERT INTO tbl_user (nama_user, password ) VALUES ($1, $2) RETURNING *`,
    [nama_user, hashPassword],
    (error, results) => {
      if (error) {
        throw error;
      }
      const token = Helper.generateToken(results.rows[0].id_user);
      response.status(201).send({ token });
    }
  );
};

const loginUser = (request, response) => {
  const { nama_user } = request.body;
  const text = "SELECT * FROM tbl_user WHERE nama_user = $1";
  pool.query(text, [nama_user], (error, results) => {
    const { password } = request.body;
    if (error) {
      throw error;
    }
    if (!Helper.comparePassword(results.rows[0].password, password)) {
      return response.status(400).send({ message: "Wrong Password" });
    }
    const token = Helper.generateToken(results.rows[0].id_user);
    response.status(201).send({id_user:results.rows[0].id_user, token });
  });
};

const updateUser = (request, response) => {
  const id = parseInt(request.params.userId);
  const { nama_user, password } = request.body;

  pool.query(
    "UPDATE tbl_user SET nama_user = $1, password = $2 WHERE id_user = $3",
    [nama_user, password, id],
    (error, results) => {
      if (error) {
        throw error;
      }
      response.status(200).send(`User modified with ID: ${id}`);
    }
  );
};

module.exports = {
  getUsers,
  getUserById,
  createUser,
  updateUser,
  loginUser,
};
