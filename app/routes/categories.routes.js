module.exports = (app) => {
	const categories = require("../models/categories.model.js");
	const { Helper } = require("../controllers/helper.js");
	app.post("/categories", Helper.verifyToken, categories.createCategorie);
	app.get("/categories", Helper.verifyToken, categories.getCategories);
	app.get(
		"/categories/:categoriesId",
		Helper.verifyToken,
		categories.getCategorieById
	);
	app.put(
		"/categories/:categoriesId",
		Helper.verifyToken,
		categories.updateCategorie
	);
	app.delete(
		"/categories/:categoriesId",
		Helper.verifyToken,
		categories.deleteCategorie
	);
};
