module.exports = (app) => {
	const product = require("../models/product.model.js");
	const { Helper } = require("../controllers/helper.js");
	app.post("/product",Helper.verifyToken, product.createProduct);
	app.get("/product",Helper.verifyToken, product.getProducts);
	app.get("/product/:productId",Helper.verifyToken, product.getProductById);
	app.put("/product/:productId",Helper.verifyToken, product.updateProduct);
	app.delete("/product/:productId",Helper.verifyToken, product.deleteProduct);
};

