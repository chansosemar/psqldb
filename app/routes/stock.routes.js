module.exports = (app) => {
	const stock = require("../models/stock.model.js");
	const { Helper } = require("../controllers/helper.js");
	app.post("/stock",Helper.verifyToken, stock.createStock);
	app.get("/stock",Helper.verifyToken, stock.getStocks);
	app.get("/stock/:stockId",Helper.verifyToken, stock.getStockById);
	app.put("/stock/:stockId",Helper.verifyToken, stock.updateStock);
	app.delete("/stock/:stockId",Helper.verifyToken, stock.deleteStock);
};

