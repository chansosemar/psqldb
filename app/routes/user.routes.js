module.exports = (app) => {
	const user = require("../models/user.model.js");
	const { Helper } = require("../controllers/helper.js");
	const cors = require("cors");
	app.post("/register", user.createUser);
	app.post("/login",user.loginUser);
	app.get("/user",Helper.verifyToken, user.getUsers);
	app.get("/user/:userId",Helper.verifyToken, user.getUserById);
	app.put("/user/:userId",Helper.verifyToken, user.updateUser);
};
