CREATE TABLE IF NOT EXISTS tbl_user (
	id_user SERIAL PRIMARY KEY,
	nama_user VARCHAR(200) NOT NULL,
	password VARCHAR(200) NOT NULL
	);

CREATE TABLE IF NOT EXISTS tbl_kategori (
	id_kategori SERIAL PRIMARY KEY,
	nama_kategori VARCHAR(200) NOT NULL
	);

CREATE TABLE IF NOT EXISTS tbl_produk (
	id_produk SERIAL PRIMARY KEY,
	kategori_id INTEGER NOT NULL,
	nama_produk VARCHAR(200) NOT NULL,
	kode_produk VARCHAR(50) NOT NULL,
	foto_produk VARCHAR(200) NOT NULL,
	tgl_register TIMESTAMPTZ NOT NULL DEFAULT CURRENT_TIMESTAMP,
	FOREIGN KEY (kategori_id) REFERENCES tbl_kategori (id_kategori) ON DELETE CASCADE
	);



CREATE OR REPLACE FUNCTION trigger_set_timestamp()
RETURNS TRIGGER AS $$
BEGIN
  NEW.tgl_update = NOW();
  RETURN NEW;
END;
$$ LANGUAGE plpgsql;


CREATE TABLE IF NOT EXISTS tbl_stok (
	id_stok SERIAL PRIMARY KEY,
	produk_id INTEGER NOT NULL,
	jumlah_barang INTEGER NOT NULL,
	tgl_update TIMESTAMPTZ NOT NULL DEFAULT NOW(),
	FOREIGN KEY (produk_id) REFERENCES tbl_produk (id_produk) ON DELETE CASCADE
	);

CREATE TRIGGER set_timestamp
BEFORE UPDATE ON tbl_stok
FOR EACH ROW
EXECUTE PROCEDURE trigger_set_timestamp();