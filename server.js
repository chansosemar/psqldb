const express = require("express");
const bodyParser = require("body-parser");
const cors = require("cors");
const dbConfig = require("./app/config/db.config.js");
const app = express();
require("dotenv").config();

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

// const corsOptions = {
// 	origin: "*",
// 	allowedHeaders: "*",
// 	optionsSuccessStatus: 200,
// 	method: "GET,HEAD ,PUT, PATCH, POST, DELETE, OPTIONS",
// };
// 
// app.use(cors(corsOptions));


app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});

app.get("/", (req, res) => {
	res.json({
		message: "Welcome to inventory db app",
	});
});

require("./app/routes/user.routes.js")(app);
require("./app/routes/product.routes.js")(app);
require("./app/routes/categories.routes.js")(app);
require("./app/routes/stock.routes.js")(app);

const port = process.env.PORT || 3000;

app.listen(port, () => {
	console.log(`server running on ${port}`);
});
